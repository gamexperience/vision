<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230705133715 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE criminal ADD amount_money_settled DOUBLE PRECISION DEFAULT NULL, ADD amount_time_settled INT DEFAULT NULL, ADD amount_community_work_settled INT DEFAULT NULL');
        $this->addSql('UPDATE criminal SET amount_money_settled = amount_money, amount_time_settled = amount_time, amount_community_work_settled = amount_community_work');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE criminal DROP amount_money_settled, DROP amount_time_settled, DROP amount_community_work_settled');
    }
}
