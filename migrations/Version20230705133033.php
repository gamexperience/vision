<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230705133033 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE criminal ADD amount_community_work INT AFTER amount_time');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE criminal DROP amount_community_work');
    }
}
