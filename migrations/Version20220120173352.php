<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220120173352 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE announce (id INT NOT NULL, content LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bracelet (id INT NOT NULL, directory_id INT NOT NULL, removing_date DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_93F6777D2C94069F (directory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE certificate (id INT NOT NULL, directory_id INT NOT NULL, content LONGTEXT NOT NULL, INDEX IDX_219CDA4A2C94069F (directory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comment (id INT AUTO_INCREMENT NOT NULL, document_id INT NOT NULL, creator_id INT NOT NULL, main_group_id INT NOT NULL, content LONGTEXT NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_9474526CC33F7837 (document_id), INDEX IDX_9474526C61220EA6 (creator_id), INDEX IDX_9474526C2776C832 (main_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE complaint (id INT NOT NULL, directory_id INT NOT NULL, content LONGTEXT NOT NULL, versus VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, INDEX IDX_5F2732B52C94069F (directory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE criminal (id INT NOT NULL, directory_id INT NOT NULL, article VARCHAR(255) NOT NULL, amount_money DOUBLE PRECISION DEFAULT NULL, amount_time INT DEFAULT NULL, content LONGTEXT DEFAULT NULL, accessory_sentence LONGTEXT DEFAULT NULL, INDEX IDX_326D7D6B2C94069F (directory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE directory (id INT AUTO_INCREMENT NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, gender VARCHAR(255) NOT NULL, birthdate DATE DEFAULT NULL, phone VARCHAR(255) DEFAULT NULL, address LONGTEXT DEFAULT NULL, height DOUBLE PRECISION DEFAULT NULL, weight DOUBLE PRECISION DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', face_image_name VARCHAR(255) DEFAULT NULL, face_image_size INT DEFAULT NULL, back_image_name VARCHAR(255) DEFAULT NULL, back_image_size INT DEFAULT NULL, left_image_name VARCHAR(255) DEFAULT NULL, left_image_size INT DEFAULT NULL, right_image_name VARCHAR(255) DEFAULT NULL, right_image_size INT DEFAULT NULL, car_licence_image_name VARCHAR(255) DEFAULT NULL, car_licence_image_size INT DEFAULT NULL, motorcycle_licence_image_name VARCHAR(255) DEFAULT NULL, motorcycle_licence_image_size INT DEFAULT NULL, truck_licence_image_name VARCHAR(255) DEFAULT NULL, truck_licence_image_size INT DEFAULT NULL, boat_licence_image_name VARCHAR(255) DEFAULT NULL, boat_licence_image_size INT DEFAULT NULL, id_card_image_name VARCHAR(255) DEFAULT NULL, id_card_image_size INT DEFAULT NULL, medical_family_history LONGTEXT DEFAULT NULL, medical_history LONGTEXT DEFAULT NULL, medical_allergies LONGTEXT DEFAULT NULL, medical_blood_group VARCHAR(255) DEFAULT NULL, medical_drugs LONGTEXT DEFAULT NULL, medical_alcohol LONGTEXT DEFAULT NULL, medical_treatment LONGTEXT DEFAULT NULL, wanted TINYINT(1) DEFAULT \'0\' NOT NULL, wanted_reason VARCHAR(255) DEFAULT NULL, dead TINYINT(1) DEFAULT \'0\' NOT NULL, face_image_date DATETIME DEFAULT NULL, back_image_date DATETIME DEFAULT NULL, left_image_date DATETIME DEFAULT NULL, right_image_date DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document (id INT AUTO_INCREMENT NOT NULL, creator_id INT NOT NULL, main_group_id INT NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', share VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, allow_share TINYINT(1) DEFAULT \'0\' NOT NULL, archive TINYINT(1) DEFAULT \'0\' NOT NULL, need_legal_access TINYINT(1) DEFAULT \'0\' NOT NULL, need_medical_access TINYINT(1) DEFAULT \'0\' NOT NULL, need_group_administration TINYINT(1) DEFAULT \'0\' NOT NULL, dtype VARCHAR(255) NOT NULL, INDEX IDX_D8698A7661220EA6 (creator_id), INDEX IDX_D8698A762776C832 (main_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document_group (document_id INT NOT NULL, group_id INT NOT NULL, INDEX IDX_46132B7C33F7837 (document_id), INDEX IDX_46132B7FE54D947 (group_id), PRIMARY KEY(document_id, group_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ext_log_entries (id INT AUTO_INCREMENT NOT NULL, action VARCHAR(8) NOT NULL, logged_at DATETIME NOT NULL, object_id VARCHAR(64) DEFAULT NULL, object_class VARCHAR(191) NOT NULL, version INT NOT NULL, data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', username VARCHAR(191) DEFAULT NULL, INDEX log_class_lookup_idx (object_class), INDEX log_date_lookup_idx (logged_at), INDEX log_user_lookup_idx (username), INDEX log_version_lookup_idx (object_id, object_class, version), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB ROW_FORMAT = DYNAMIC');
        $this->addSql('CREATE TABLE gang (id INT NOT NULL, informations LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `group` (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', short_name VARCHAR(12) NOT NULL, motd LONGTEXT DEFAULT NULL, motd_updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', image_name VARCHAR(255) DEFAULT NULL, image_size INT DEFAULT NULL, updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE infringement (id INT NOT NULL, directory_id INT NOT NULL, content LONGTEXT NOT NULL, INDEX IDX_B5682C422C94069F (directory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE jail (id INT NOT NULL, directory_id INT NOT NULL, arrested_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', jailed_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', content LONGTEXT NOT NULL, lawyer TINYINT(1) DEFAULT \'0\' NOT NULL, medic TINYINT(1) DEFAULT \'0\' NOT NULL, INDEX IDX_CCF194F12C94069F (directory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE licence_withdrawal (id INT NOT NULL, directory_id INT NOT NULL, type VARCHAR(255) NOT NULL, until DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_CE2562292C94069F (directory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE medical (id INT NOT NULL, directory_id INT NOT NULL, content LONGTEXT NOT NULL, INDEX IDX_77DB075A2C94069F (directory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notification (id INT AUTO_INCREMENT NOT NULL, sender_id INT DEFAULT NULL, receiver_id INT NOT NULL, icon VARCHAR(255) NOT NULL, content VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', readed TINYINT(1) DEFAULT \'0\' NOT NULL, INDEX IDX_BF5476CAF624B39D (sender_id), INDEX IDX_BF5476CACD53EDB6 (receiver_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rank (id INT AUTO_INCREMENT NOT NULL, main_group_id INT NOT NULL, name VARCHAR(255) NOT NULL, shortname VARCHAR(255) NOT NULL, power INT NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', permissions LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', INDEX IDX_8879E8E52776C832 (main_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE report (id INT NOT NULL, content LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reset_password_request (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', expires_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_7CE748AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sanction (id INT NOT NULL, user_id INT NOT NULL, content LONGTEXT NOT NULL, INDEX IDX_6D6491AFA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stolenvehicle (id INT NOT NULL, directory_id INT NOT NULL, type VARCHAR(255) NOT NULL, numberplate VARCHAR(255) DEFAULT NULL, model VARCHAR(255) DEFAULT NULL, content LONGTEXT DEFAULT NULL, color VARCHAR(255) DEFAULT NULL, INDEX IDX_728827D92C94069F (directory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE template (id INT NOT NULL, content LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, main_group_id INT DEFAULT NULL, main_rank_id INT DEFAULT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, is_verified TINYINT(1) DEFAULT \'0\' NOT NULL, admin_mode TINYINT(1) DEFAULT \'0\' NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, phone VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', is_desactivated TINYINT(1) DEFAULT \'0\' NOT NULL, locale VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), INDEX IDX_8D93D6492776C832 (main_group_id), INDEX IDX_8D93D649825DCE66 (main_rank_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE announce ADD CONSTRAINT FK_E6D6DD75BF396750 FOREIGN KEY (id) REFERENCES document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE bracelet ADD CONSTRAINT FK_93F6777D2C94069F FOREIGN KEY (directory_id) REFERENCES directory (id)');
        $this->addSql('ALTER TABLE bracelet ADD CONSTRAINT FK_93F6777DBF396750 FOREIGN KEY (id) REFERENCES document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE certificate ADD CONSTRAINT FK_219CDA4A2C94069F FOREIGN KEY (directory_id) REFERENCES directory (id)');
        $this->addSql('ALTER TABLE certificate ADD CONSTRAINT FK_219CDA4ABF396750 FOREIGN KEY (id) REFERENCES document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CC33F7837 FOREIGN KEY (document_id) REFERENCES document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C61220EA6 FOREIGN KEY (creator_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C2776C832 FOREIGN KEY (main_group_id) REFERENCES `group` (id)');
        $this->addSql('ALTER TABLE complaint ADD CONSTRAINT FK_5F2732B52C94069F FOREIGN KEY (directory_id) REFERENCES directory (id)');
        $this->addSql('ALTER TABLE complaint ADD CONSTRAINT FK_5F2732B5BF396750 FOREIGN KEY (id) REFERENCES document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE criminal ADD CONSTRAINT FK_326D7D6B2C94069F FOREIGN KEY (directory_id) REFERENCES directory (id)');
        $this->addSql('ALTER TABLE criminal ADD CONSTRAINT FK_326D7D6BBF396750 FOREIGN KEY (id) REFERENCES document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A7661220EA6 FOREIGN KEY (creator_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A762776C832 FOREIGN KEY (main_group_id) REFERENCES `group` (id)');
        $this->addSql('ALTER TABLE document_group ADD CONSTRAINT FK_46132B7C33F7837 FOREIGN KEY (document_id) REFERENCES document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE document_group ADD CONSTRAINT FK_46132B7FE54D947 FOREIGN KEY (group_id) REFERENCES `group` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE gang ADD CONSTRAINT FK_E6080363BF396750 FOREIGN KEY (id) REFERENCES document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE infringement ADD CONSTRAINT FK_B5682C422C94069F FOREIGN KEY (directory_id) REFERENCES directory (id)');
        $this->addSql('ALTER TABLE infringement ADD CONSTRAINT FK_B5682C42BF396750 FOREIGN KEY (id) REFERENCES document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE jail ADD CONSTRAINT FK_CCF194F12C94069F FOREIGN KEY (directory_id) REFERENCES directory (id)');
        $this->addSql('ALTER TABLE jail ADD CONSTRAINT FK_CCF194F1BF396750 FOREIGN KEY (id) REFERENCES document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE licence_withdrawal ADD CONSTRAINT FK_CE2562292C94069F FOREIGN KEY (directory_id) REFERENCES directory (id)');
        $this->addSql('ALTER TABLE licence_withdrawal ADD CONSTRAINT FK_CE256229BF396750 FOREIGN KEY (id) REFERENCES document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE medical ADD CONSTRAINT FK_77DB075A2C94069F FOREIGN KEY (directory_id) REFERENCES directory (id)');
        $this->addSql('ALTER TABLE medical ADD CONSTRAINT FK_77DB075ABF396750 FOREIGN KEY (id) REFERENCES document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CAF624B39D FOREIGN KEY (sender_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CACD53EDB6 FOREIGN KEY (receiver_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE rank ADD CONSTRAINT FK_8879E8E52776C832 FOREIGN KEY (main_group_id) REFERENCES `group` (id)');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F7784BF396750 FOREIGN KEY (id) REFERENCES document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE sanction ADD CONSTRAINT FK_6D6491AFA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE sanction ADD CONSTRAINT FK_6D6491AFBF396750 FOREIGN KEY (id) REFERENCES document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stolenvehicle ADD CONSTRAINT FK_728827D92C94069F FOREIGN KEY (directory_id) REFERENCES directory (id)');
        $this->addSql('ALTER TABLE stolenvehicle ADD CONSTRAINT FK_728827D9BF396750 FOREIGN KEY (id) REFERENCES document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE template ADD CONSTRAINT FK_97601F83BF396750 FOREIGN KEY (id) REFERENCES document (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6492776C832 FOREIGN KEY (main_group_id) REFERENCES `group` (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649825DCE66 FOREIGN KEY (main_rank_id) REFERENCES rank (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bracelet DROP FOREIGN KEY FK_93F6777D2C94069F');
        $this->addSql('ALTER TABLE certificate DROP FOREIGN KEY FK_219CDA4A2C94069F');
        $this->addSql('ALTER TABLE complaint DROP FOREIGN KEY FK_5F2732B52C94069F');
        $this->addSql('ALTER TABLE criminal DROP FOREIGN KEY FK_326D7D6B2C94069F');
        $this->addSql('ALTER TABLE infringement DROP FOREIGN KEY FK_B5682C422C94069F');
        $this->addSql('ALTER TABLE jail DROP FOREIGN KEY FK_CCF194F12C94069F');
        $this->addSql('ALTER TABLE licence_withdrawal DROP FOREIGN KEY FK_CE2562292C94069F');
        $this->addSql('ALTER TABLE medical DROP FOREIGN KEY FK_77DB075A2C94069F');
        $this->addSql('ALTER TABLE stolenvehicle DROP FOREIGN KEY FK_728827D92C94069F');
        $this->addSql('ALTER TABLE announce DROP FOREIGN KEY FK_E6D6DD75BF396750');
        $this->addSql('ALTER TABLE bracelet DROP FOREIGN KEY FK_93F6777DBF396750');
        $this->addSql('ALTER TABLE certificate DROP FOREIGN KEY FK_219CDA4ABF396750');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CC33F7837');
        $this->addSql('ALTER TABLE complaint DROP FOREIGN KEY FK_5F2732B5BF396750');
        $this->addSql('ALTER TABLE criminal DROP FOREIGN KEY FK_326D7D6BBF396750');
        $this->addSql('ALTER TABLE document_group DROP FOREIGN KEY FK_46132B7C33F7837');
        $this->addSql('ALTER TABLE gang DROP FOREIGN KEY FK_E6080363BF396750');
        $this->addSql('ALTER TABLE infringement DROP FOREIGN KEY FK_B5682C42BF396750');
        $this->addSql('ALTER TABLE jail DROP FOREIGN KEY FK_CCF194F1BF396750');
        $this->addSql('ALTER TABLE licence_withdrawal DROP FOREIGN KEY FK_CE256229BF396750');
        $this->addSql('ALTER TABLE medical DROP FOREIGN KEY FK_77DB075ABF396750');
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F7784BF396750');
        $this->addSql('ALTER TABLE sanction DROP FOREIGN KEY FK_6D6491AFBF396750');
        $this->addSql('ALTER TABLE stolenvehicle DROP FOREIGN KEY FK_728827D9BF396750');
        $this->addSql('ALTER TABLE template DROP FOREIGN KEY FK_97601F83BF396750');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C2776C832');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A762776C832');
        $this->addSql('ALTER TABLE document_group DROP FOREIGN KEY FK_46132B7FE54D947');
        $this->addSql('ALTER TABLE rank DROP FOREIGN KEY FK_8879E8E52776C832');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6492776C832');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649825DCE66');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C61220EA6');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A7661220EA6');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CAF624B39D');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CACD53EDB6');
        $this->addSql('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748AA76ED395');
        $this->addSql('ALTER TABLE sanction DROP FOREIGN KEY FK_6D6491AFA76ED395');
        $this->addSql('DROP TABLE announce');
        $this->addSql('DROP TABLE bracelet');
        $this->addSql('DROP TABLE certificate');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE complaint');
        $this->addSql('DROP TABLE criminal');
        $this->addSql('DROP TABLE directory');
        $this->addSql('DROP TABLE document');
        $this->addSql('DROP TABLE document_group');
        $this->addSql('DROP TABLE ext_log_entries');
        $this->addSql('DROP TABLE gang');
        $this->addSql('DROP TABLE `group`');
        $this->addSql('DROP TABLE infringement');
        $this->addSql('DROP TABLE jail');
        $this->addSql('DROP TABLE licence_withdrawal');
        $this->addSql('DROP TABLE medical');
        $this->addSql('DROP TABLE notification');
        $this->addSql('DROP TABLE rank');
        $this->addSql('DROP TABLE report');
        $this->addSql('DROP TABLE reset_password_request');
        $this->addSql('DROP TABLE sanction');
        $this->addSql('DROP TABLE stolenvehicle');
        $this->addSql('DROP TABLE template');
        $this->addSql('DROP TABLE user');
    }
}
