<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220615174010 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE directory ADD medical_contact LONGTEXT DEFAULT NULL, ADD medicaltrusted LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE licencewithdrawal DROP FOREIGN KEY FK_CE2562292C94069F');
        $this->addSql('DROP INDEX idx_ce2562292c94069f ON licencewithdrawal');
        $this->addSql('CREATE INDEX IDX_94E8823A2C94069F ON licencewithdrawal (directory_id)');
        $this->addSql('ALTER TABLE licencewithdrawal ADD CONSTRAINT FK_CE2562292C94069F FOREIGN KEY (directory_id) REFERENCES directory (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE directory DROP medical_contact, DROP medicaltrusted');
        $this->addSql('ALTER TABLE licencewithdrawal DROP FOREIGN KEY FK_94E8823A2C94069F');
        $this->addSql('DROP INDEX idx_94e8823a2c94069f ON licencewithdrawal');
        $this->addSql('CREATE INDEX IDX_CE2562292C94069F ON licencewithdrawal (directory_id)');
        $this->addSql('ALTER TABLE licencewithdrawal ADD CONSTRAINT FK_94E8823A2C94069F FOREIGN KEY (directory_id) REFERENCES directory (id)');
    }
}
