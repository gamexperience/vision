<?php

namespace App\Command;

use App\Entity\Group;
use App\Entity\Rank;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'vision:addrank', description: 'Add a rank to a vision\'s group')]
class AddRankCommand extends Command
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
    }

    protected function configure(): void
    {
        $this
        ->addArgument('groupid', InputArgument::OPTIONAL, 'Id of the main group of the user')
        ->addArgument('name', InputArgument::OPTIONAL, 'Name of the Rank')
        ->addArgument('shortname', InputArgument::OPTIONAL, 'Short name of the Rank')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $helper = $this->getHelper('question');

        $groupid = $input->getArgument('groupid');
        /**
         * @var GroupRepository $groupRepository
         */
        $groupRepository = $this->entityManager->getRepository(Group::class);

        if ($groupid && null === $groupRepository->find($groupid)) {
            $groupid = '';
        }

        if (!$groupid) {
            foreach ($groupRepository->findAll() as $key => $g) {
                $io->info('"' . $g->getName() . '" id: ' . $g->getId());
            }


            $questionGroup = new Question('Please enter the rank\'s group ID: ', '');
            $questionGroup->setValidator(function ($answer) {
                if (!is_numeric($answer) || empty($answer)) {
                    throw new \RuntimeException(
                        'The group id c\'ant be empty'
                    );
                }
                global $groupRepository;
                if (null === $this->entityManager->getRepository(Group::class)->find($answer)) {
                    throw new \RuntimeException(
                        'No group find for this id'
                    );
                }

                return $answer;
            });
            $groupid = $helper->ask($input, $output, $questionGroup);
        }

        $group = $groupRepository->find($groupid);

        $rankName = $input->getArgument('name');
        if (!$rankName) {
            $questionName = new Question('Please enter the name of the rank : ', '');
            $questionName->setValidator(function ($answer) {
                if (!is_string($answer) || empty($answer)) {
                    throw new \RuntimeException(
                        'The name c\'ant be empty'
                    );
                }

                return $answer;
            });
            $rankName = $helper->ask($input, $output, $questionName);
        }

        $groupShortName = $input->getArgument('shortname');
        if (!$groupShortName) {
            $questionShortName = new Question('Please enter the short name of the rank : ', '');
            $questionShortName->setValidator(function ($answer) {
                if (!is_string($answer) || empty($answer)) {
                    throw new \RuntimeException(
                        'The shortname c\'ant be empty'
                    );
                }

                    return $answer;
            });
            $groupShortName = $helper->ask($input, $output, $questionShortName);
        }

        $Rank = new Rank();
        $Rank->setName($rankName);
        $Rank->setShortname($groupShortName);
        $Rank->setMainGroup($group);
        $Rank->setPower(0);
        $this->entityManager->persist($Rank);

        try {
            $this->entityManager->flush();
            $io->success('Group ' . $Rank->getName()  . ' created');
            return Command::SUCCESS ;
        } catch (\Throwable $th) {
            throw $th;
            $io->error('Error while creating ' . $Rank->getName()  . ' group');
            return Command::FAILURE;
        }
    }
}
