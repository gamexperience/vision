<?php

namespace App\Repository;

use App\Entity\Complaint;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\Tools\DocumentRepositoriesExtension;

/**
 * @method Complaint|null find($id, $lockMode = null, $lockVersion = null)
 * @method Complaint|null findOneBy(array $criteria, array $orderBy = null)
 * @method Complaint[]    findAll()
 * @method Complaint[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComplaintRepository extends DocumentRepositoriesExtension
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Complaint::class);
        $this->fields = ['content', 'versus']; //with title, list fields we can search in
    }
}
