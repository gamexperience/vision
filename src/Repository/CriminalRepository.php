<?php

namespace App\Repository;

use App\Entity\Criminal;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\Tools\DocumentRepositoriesExtension;

/**
 * @method Criminal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Criminal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Criminal[]    findAll()
 * @method Criminal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CriminalRepository extends DocumentRepositoriesExtension
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Criminal::class);
        $this->fields = ['amountMoney', 'amountTime', 'content']; //with title, list fields we can search in
    }

    public function limitUnsettled(string $type)
    {
        if ($type == Criminal::TYPE_FINE) {
            $column = 'd.amountMoney';
        } elseif ($type == Criminal::TYPE_JAIL) {
            $column = 'd.amountTime';
        } elseif ($type == Criminal::TYPE_COMMUNITY_WORK) {
            $column = 'd.amountCommunityWork';
        } else {
            throw new \OutOfRangeException($type);
        }

        $settledColumn = $column . 'Settled';

        $this->qb->andWhere("$column <> $settledColumn OR ($column IS NOT NULL AND $settledColumn IS NULL)");

        return $this;
    }
}
