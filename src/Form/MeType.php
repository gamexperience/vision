<?php

namespace App\Form;

use App\Entity\User;
use App\Form\Type\PhoneType;
use App\Form\Type\LastnameType;
use App\Form\Type\FirstnameType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class MeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('firstname', FirstnameType::class, ['disabled' => true, 'help' => 'form_help_cant_edit_firstname'])
            ->add('lastname', LastnameType::class, ['disabled' => true, 'help' => 'form_help_cant_edit_lastname'])
            ->add('email', EmailType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'form_placeholder_email'],
            ])
            ->add('phone', PhoneType::class)
            ->add('locale', ChoiceType::class, [
                'choices' => [
                    'form_choice_default' => null,
                    'form_choice_english' => 'en',
                    'form_choice_french' => 'fr'
                ],
                'expanded' => false,
                'multiple' => false,
                'label' => 'form_label_language',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'form_button_update',
                'attr' => ['class' => 'btn-primary'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
