<?php

namespace App\Form;

use App\Entity\Criminal;
use App\Form\DocumentType;
use App\Form\Type\ContentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CriminalType extends DocumentType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        parent::buildForm($builder, $options);

        $builder

            ->add('content', ContentType::class, ['label' => 'form_label_informations' ])
            ->add('amountMoney', null, ['label' => 'form_label_amount', 'help' => 'form_help_amount'])
            ->add('amountTime', null, ['label' => 'form_label_time', 'help' => 'form_help_time'])
            ->add(
                'amountCommunityWork',
                null,
                ['label' => 'form_label_community_work', 'help' => 'form_help_community_work']
            )
            ->add(
                'amountMoneySettled',
                null,
                ['label' => 'form_label_amount_settled', 'help' => 'form_help_amount_settled']
            )
            ->add(
                'amountTimeSettled',
                null,
                ['label' => 'form_label_time_settled', 'help' => 'form_help_time_settled']
            )
            ->add(
                'amountCommunityWorkSettled',
                null,
                ['label' => 'form_label_community_work_settled', 'help' => 'form_help_community_work_settled']
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Criminal::class,
        ]);
    }
}
