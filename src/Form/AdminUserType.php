<?php

namespace App\Form;

use App\Entity\Rank;
use App\Entity\User;
use App\Entity\SubGroup;
use App\Form\Type\PhoneType;
use App\Form\Type\LastnameType;
use App\Form\Type\FirstnameType;
use App\Repository\RankRepository;
use App\Repository\SubGroupRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class AdminUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        /**
         * @var User $User
         */
        $User = $builder->getData();
        $UserGroup = $User->getMainGroup();

        $builder
            ->add('email', null, ['label' => 'form_label_email'])
            ->add('firstname', FirstnameType::class)
            ->add('lastname', LastnameType::class)
            ->add('phone', PhoneType::class)
            ->add('mainRank', EntityType::class, [
                'class' => Rank::class,
                'query_builder' => function (RankRepository $RankRepository) {
                    return $RankRepository->createQueryBuilder('r')
                        ->orderBy('r.power', 'DESC');
                },
                'choice_label' => 'name',
                'label' => 'form_label_group_and_rank',
                'required' => false,
                'group_by' => function ($choice, $key, $value) {
                    return $choice->getMainGroup()->getName();
                },
            ]);

        if ($UserGroup != null && !$UserGroup->getSubGroups()->isEmpty()) {
            $builder
            ->add('subGroups', EntityType::class, [
                'class' => SubGroup::class,
                'query_builder' => function (SubGroupRepository $SubGroupRepository) use ($UserGroup) {
                    return $SubGroupRepository->createQueryBuilder('sg')
                        ->where('sg.mainGroup =' . $UserGroup->getId())
                        ;
                },
                'choice_label' => 'name',
                'label' => 'form_label_subgroups',
                'required' => false,
                'multiple' => true,
                'expanded' => true
            ]);
        }


            $builder
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    'form_choice_admin' => 'ROLE_ADMIN'
                ],
                'expanded' => true,
                'multiple' => true,
                'label' => 'form_label_roles',
            ])
            ->add('isVerified', CheckboxType::class, [
                'label' => 'form_label_checkbox_isverified',
                'help' => 'form_help_checkbox_isverified',
                'required' => false
            ])
            ->add('isDesactivated', CheckboxType::class, [
                'label' => 'form_label_checkbox_isdesactivated',
                'help' => 'form_help_checkbox_isdesactivated',
                'required' => false
            ])
            ->add('adminMode', CheckboxType::class, [
                'label' => 'form_label_checkbox_adminmode',
                'help' => 'form_help_checkbox_adminmode',
                'required' => false
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'form_button_submit',
                'attr' => ['class' => 'btn-primary'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
