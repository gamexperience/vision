<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class RepeatedPasswordType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'type' => PasswordType::class,
            'mapped' => false,
            'invalid_message' => 'form_constraint_password_mismatch',
            'options' => ['attr' => ['class' => 'password-field']],
            'required' => true,
            'first_options'  => ['label' => false, 'attr' => ['placeholder' => 'form_placeholder_password']],
            'second_options' => ['label' => false, 'attr' => ['placeholder' => 'form_placeholder_password_repeat']],
            'constraints' => [
                new NotBlank([
                    'message' => 'form_constraint_please_enter_password',
                ]),
                new Length([
                    'min' => 6,
                    'minMessage' => 'form_constraint_password_min_lenght_6_cars',
                    // max length allowed by Symfony for security reasons
                    'max' => 4096,
                ]),
            ]
        ]);
    }

    public function getParent(): string
    {
        return RepeatedType::class;
    }
}
