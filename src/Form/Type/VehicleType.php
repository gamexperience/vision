<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class VehicleType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
                'choices' => [
                    'form_choice_car' => 'form_choice_car',
                    'form_choice_motorcycle' => 'form_choice_motorcycle',
                    'form_choice_plane' => 'form_choice_plane',
                    'form_choice_boat' => 'form_choice_boat',
                    'form_choice_helicopter' => 'form_choice_helicopter',
                    'form_choice_bike' => 'form_choice_bike',
                    'form_choice_truck' => 'form_choice_truck',
                    'form_choice_other' => 'form_choice_other'
                ],
                'label' => 'form_label_vehicle_type',
        ]);
    }

    public function getParent(): string
    {
        return ChoiceType::class;
    }
}
