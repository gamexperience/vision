<?php

namespace App\Form\Type;

use App\Entity\SubGroup;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Security\Core\Security;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserSubGroupsType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {

        /**
         * @var User $user
         */
        $user = $this->security->getUser();


        $resolver->setDefaults([
            'class' => SubGroup::class,
            'choices' => $user->getSubGroups(),
            'multiple' => true,
            'expanded' => true,
            'choice_label' => 'name',
            'label' => 'form_label_allowedsubgroups',
            'help' => 'form_help_allowedsubgroups',
        ]);
    }

    public function getParent(): string
    {
        return EntityType::class;
    }
}
