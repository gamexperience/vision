<?php

namespace App\Form;

use App\Entity\Group;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AdminGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, ['label' => 'form_label_name'])
            ->add('shortName', null, ['label' => 'form_label_shortname'])
            ->add('imageFile', VichImageType::class, [
                'required' => false,
                'label' => 'form_label_group_logo',
                'allow_delete' => true,
                'delete_label' => 'form_label_remove_image',
                'download_label' => 'form_label_download_image',
                'download_uri' => false,
                'image_uri' => false,
                'asset_helper' => true,
            ])
            ->add('motd', null, ['label' => 'form_label_motd','attr' => ['style' => 'height: 400px;']])
            ->add('submit', SubmitType::class, [
                'label' => 'form_button_submit',
                'attr' => ['class' => 'btn-primary'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Group::class,
        ]);
    }
}
