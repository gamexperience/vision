<?php

namespace App\Form;

use App\Entity\Document;
use App\Form\Type\AllowedGroupsType;
use Symfony\Component\Form\AbstractType;
use App\Form\Type\UserGroupSubGroupsType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class DocumentType extends AbstractType
{
    private TokenStorageInterface $TokenStorage;

    public function __construct(TokenStorageInterface $TokenStorage)
    {
        $this->TokenStorage = $TokenStorage;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /**
         * @var Document $Document
         */
        $Document = $builder->getData();

        /**
         * @var User $User
         */
        $User = $this->TokenStorage->getToken()->getUser();

        $builder
        ->add('title', null, [
            'label' => 'form_label_title',
            'priority' => 999
        ]);

        if (!$Document->getIsPublic()) {
            if ($Document->getMainGroup() == $User->getMainGroup()) {
                $builder
                    ->add(
                        'allowedGroups',
                        AllowedGroupsType::class,
                        [
                            'priority' => -900,
                        ]
                    )
                    ->add(
                        'allowedSubGroups',
                        UserGroupSubGroupsType::class,
                        [
                            'priority' => -900
                        ]
                    );
            }
        }







        $builder
        ->add(
            'allowShare',
            null,
            [
                'priority' => -900,
                'label' => 'form_label_allowShare'
            ]
        )
        ->add('submit', SubmitType::class, [
            'label' => 'form_button_submit',
            'priority' => -900,
            'attr' => ['class' => 'btn-primary'],
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Document::class,
        ]);
    }
}
