<?php

namespace App\Form;

use App\Form\DocumentType;
use App\Entity\Stolenvehicle;
use App\Form\Type\ContentType;
use App\Form\Type\VehicleType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StolenvehicleType extends DocumentType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('type', VehicleType::class)
            ->add('numberplate', null, ['label' => 'form_label_numberplate'])
            ->add('model', null, ['label' => 'form_label_model'])
            ->add('color', null, ['label' => 'form_label_color'])
            ->add('content', ContentType::class, ['label' => 'form_label_informations', 'required'   => false])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Stolenvehicle::class,
        ]);
    }
}
