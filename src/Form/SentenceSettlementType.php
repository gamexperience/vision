<?php

namespace App\Form;

use App\Entity\Document;
use App\Form\Type\AllowedGroupsType;
use Symfony\Component\Form\AbstractType;
use App\Form\Type\UserGroupSubGroupsType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class SentenceSettlementType extends AbstractType
{
    private TokenStorageInterface $TokenStorage;

    public function __construct(TokenStorageInterface $TokenStorage)
    {
        $this->TokenStorage = $TokenStorage;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $sentence = $builder->getData();

        if ($sentence) {
            if ($sentence->getType() == "fine") {
                $amountType = NumberType::class;
                $amountLabel = 'form_label_paid';
            } else {
                $amountType = IntegerType::class;
                $amountLabel = 'form_label_executed';
            }
        } else {
            $amountType = null;
            $amountLabel = null;
        }

        $builder->add('criminal_id', HiddenType::class)
            ->add('type', HiddenType::class)
            ->add('amount', $amountType, ['label' => $amountLabel])
            ->add('submit', SubmitType::class, [
                'label' => 'form_button_submit',
                'priority' => -900,
            ])
        ;
    }

    public function getBlockPrefix()
    {
        return ''; // return an empty string here
    }
}
