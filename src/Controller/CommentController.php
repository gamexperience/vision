<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Document;
use App\Form\CommentType;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

#[Route('/comment', name: 'comment_')]
class CommentController extends AbstractController
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    #[Route('/create/{Document}', name: 'create')]
    #[IsGranted('view', subject: 'Document', message: 'granted_not_allowed_comment')]
    public function create(Document $Document, Request $request): Response
    {
        $Comment = new Comment($this->getUser());
        $form = $this->createForm(CommentType::class, $Comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $Comment->setDocument($Document);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($Comment);
            try {
                $entityManager->flush();
            } catch (\Throwable $th) {
                if ($_ENV['APP_ENV'] === 'dev') {
                    throw $th; //DEBUG
                } else {
                    $this->logger->error($th);
                }

                $this->addFlash('danger', 'alert_error_creating_comment');
                return $this->redirectToRoute('document_view', ['id' => $Document->getId()]);
            }

            $this->addFlash('success', 'alert_success_creating_comment');
            return $this->redirectToRoute('document_view', ['id' => $Document->getId()]);
        } elseif ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('warning', 'alert_error_form_post');
        }

        return $this->redirectToRoute('document_view', ['id' => $Document->getId()]);
    }

    #[Route('/delete/{id}', name: 'delete')]
    #[IsGranted('delete', subject: 'Comment', message: 'granted_not_allowed_deleting_comment')]
    public function delete(Comment $Comment): Response
    {
        if (!$this->IsGranted('view', $Comment->getDocument())) {
            throw new AccessDeniedHttpException('granted_not_allowed_delete_comment_on_unallowed_document');
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($Comment);
        try {
            $entityManager->flush();
        } catch (\Throwable $th) {
            if ($_ENV['APP_ENV'] === 'dev') {
                throw $th; //DEBUG
            }
            $this->addFlash('warning', 'alert_error_deleting_comment');
            return $this->redirectToRoute('document_view', ['id' => $Comment->getDocument()->getId()]);
        }

        $this->addFlash('success', 'alert_success_deleting_comment');
        return $this->redirectToRoute('document_view', ['id' => $Comment->getDocument()->getId()]);
    }

    #[Route('/edit/{id}', name: 'edit')]
    #[IsGranted('edit', subject: 'Comment', message: 'granted_not_allowed_editing_comment')]
    public function edit(Comment $Comment, Request $request): Response
    {
        if (!$this->IsGranted('view', $Comment->getDocument())) {
            throw new AccessDeniedHttpException('granted_not_allowed_editing_comment_on_unallowed_document');
        }

        $form = $this->createForm(CommentType::class, $Comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($Comment);
            try {
                $entityManager->flush();
            } catch (\Throwable $th) {
                if ($_ENV['APP_ENV'] === 'dev') {
                    throw $th; //DEBUG
                } else {
                    $this->logger->error($th);
                }

                $this->addFlash('danger', 'alert_error_editing_comment');
                return $this->redirectToRoute('document_view', ['id' => $Comment->getDocument()->getId()]);
            }

            $this->addFlash('success', 'alert_success_editing_comment');
            return $this->redirectToRoute('document_view', ['id' => $Comment->getDocument()->getId()]);
        } elseif ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('warning', 'alert_error_form_post');
        }

        return $this->render('comment/edit.html.twig', [
            'controller_name' => 'CommentController',
            'comment' => $Comment,
            'form' => $form->createView()
        ]);
    }
}
