<?php

namespace App\Controller;

use App\Entity\Template;
use App\Repository\TemplateRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/template', name: 'template_')]
class TemplateController extends AbstractController
{
    /** Class by Aurel  **/

    #[Route('/json/list', name: 'json_list')]
    public function templateJsonList(TemplateRepository $TemplateRepository): Response
    {
        /**
         * @var Array $templates
         */
        $templates = $TemplateRepository->listForUser($this->getUser())->archive(false)->getResult();
        $json = [];

        foreach ($templates as $template) {
            array_push($json, [
                'title' => $template->getTitle(),
                'description' => '',
                'url' => $this->generateUrl('template_json_get', ['id' => $template->getId()])
            ]);
        }

        return $this->json($json);
    }

    #[Route('/json/{id}', name: 'json_get')]
    public function templateJsonGet(Template $template): Response
    {
        return new Response($template->getContent());
    }
}
