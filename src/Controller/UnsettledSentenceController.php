<?php

namespace App\Controller;

use App\Entity\SentenceSettlement;
use App\Form\SearchBarType;
use App\Form\SentenceSettlementType;
use App\Repository\CriminalRepository;
use Knp\Component\Pager\PaginatorInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/unsettled_sentence', name: 'unsettled_sentence_')]
class UnsettledSentenceController extends AbstractController
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    #[Route('/list/{type?*}', name: 'list')]
    public function list(
        PaginatorInterface $paginator,
        Request $request,
        CriminalRepository $CriminalRepository,
        string $type = 'fine'
    ): Response {

        $searchForm = $this->createForm(SearchBarType::class);
        $searchForm->handleRequest($request);

        $pagination = $paginator->paginate(
            $CriminalRepository->listForUser($this->getUser())
                ->search((
                    $searchForm->isSubmitted()
                    && $searchForm->isValid()
                    && $searchForm->getData()['subject'] !== null
                ) ? $searchForm->getData()['subject'] : null)
                ->limitUnsettled($type)
                ->order(['createdAt' => 'DESC'])
                ->getResult(),
            $request->query->getInt('page', 1)
        );

        foreach ($pagination as $i => $criminal) {
            $sentence = new SentenceSettlement($criminal, $type);
            $form = $this->createForm(
                SentenceSettlementType::class,
                $sentence,
                ['action' => $this->generateUrl('unsettled_sentence_settle'),]
            );
            $pagination[$i]->form = $form->createView();
        }

        return $this->render('sentence/list.html.twig', [
            'controller_name' => 'AdminController',
            'type' => $type,
            'pagination' => $pagination,
            'count' => $pagination->getTotalItemCount(),
            'searchForm' => $searchForm->createView(),
        ]);
    }

    #[Route('/settle', name: 'settle')]
    public function settle(
        Request $request,
        CriminalRepository $CriminalRepository
    ) {
        $type = null;
        $sentenceForm = $this->createForm(SentenceSettlementType::class);
        $sentenceForm->handleRequest($request);

        if ($sentenceForm->isSubmitted() && $sentenceForm->isValid()) {
            $formData = $sentenceForm->getData();
            $type = $formData['type'];
            $criminal = $CriminalRepository->find($formData['criminal_id']);
            if (!$criminal) {
                $sentenceForm->addError(new FormError('error_document_not_found'));
            } else {
                try {
                    $criminal->addSettlement($type, $formData['amount']);
                } catch (\OutOfRangeException $e) {
                    $sentenceForm->addError(new FormError('error_value_out_of_bound'));
                }
            }
            if ($sentenceForm->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($criminal);
                try {
                    $entityManager->flush();
                } catch (\Throwable $th) {
                    if ($_ENV['APP_ENV'] === 'dev') {
                        throw $th; //DEBUG
                    } else {
                        $this->logger->error($th);
                    }

                    $this->addFlash('danger', 'alert_error_editing_document');
                    return $this->redirectToRoute($request->getRequestUri());
                }
            } else {
                $this->addFlash('warning', 'alert_error_form_post');
            }
        }

        return $this->redirectToRoute('unsettled_sentence_list', ['type' => $type]);
    }
}
