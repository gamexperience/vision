<?php

namespace App\Twig;

use App\Entity\User;
use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\EntityManagerInterface;

class ClassMetadataExtension extends AbstractExtension
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('classMetadataToEntity', [$this, 'classMetadataToEntity']),
            new TwigFunction('xdump', [$this, 'xdump']),
        ];
    }

    public function classMetadataToEntity(ClassMetadata $ClassMetadata, User $user)
    {

        $className = $ClassMetadata->getName();
        return new $className($user);
    }

    public function xdump($data)
    {
        var_dump($data);
    }
}
