<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class VersionExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('vision_version', [$this, 'visionVersion']),
        ];
    }

    public function visionVersion()
    {
        return json_decode(file_get_contents('../visionversion.json'))->version;
    }
}
