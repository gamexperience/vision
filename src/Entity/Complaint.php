<?php

namespace App\Entity;

use App\Entity\Document;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Repository\ComplaintRepository;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ComplaintRepository::class)
 */
class Complaint extends Document
{
    /**
     * @ORM\ManyToOne(targetEntity=Directory::class, inversedBy="complaints")
     * @ORM\JoinColumn(nullable=false)
     */
    private $directory;

    /**
     * @ORM\Column(type="text", length=4294967295)
     * @Gedmo\Versioned
     * @Assert\Length(
     *     max=65535,
     *     maxMessage="Content too long : {{ limit }} max"
     * )
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Versioned
     */
    private $versus;

    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Versioned
     */
    private $status;

    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->setNeedLegalAccess(true);
    }

    public function getDirectory(): ?Directory
    {
        return $this->directory;
    }

    public function setDirectory(?Directory $directory): self
    {
        $this->directory = $directory;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getVersus(): ?string
    {
        return $this->versus;
    }

    public function setVersus(string $versus): self
    {
        $this->versus = $versus;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;
        return $this;
    }
}
