<?php

namespace App\Entity;

class SentenceSettlement
{
    private Criminal $criminal;
    private string $type;
    private $amount = null;

    public function __construct(
        Criminal $criminal,
        string $type
    ) {
        $this->criminal = $criminal;
        $this->type = $type;
    }

    /**
     * @return Criminal
     */
    public function getCriminal(): Criminal
    {
        return $this->criminal;
    }

    /**
     * @param Criminal $criminal
     */
    public function setCriminal(Criminal $criminal): void
    {
        $this->criminal = $criminal;
    }

    /**
     * @return Criminal
     */
    public function getCriminalId(): int
    {
        return $this->criminal->getId();
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return null
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param null $amount
     */
    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }
}
