<?php

namespace App\Entity;

use OutOfRangeException;
use App\Entity\Document;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\CriminalRepository;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CriminalRepository::class)
 */
class Criminal extends Document
{
    public const TYPE_JAIL = 'jail';
    public const TYPE_FINE = 'fine';
    public const TYPE_COMMUNITY_WORK = 'community_work';

    /**
     * @ORM\ManyToOne(targetEntity=Directory::class, inversedBy="criminals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $directory;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Gedmo\Versioned
     */
    private $amountMoney;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    private $amountTime;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    private $amountCommunityWork;

    /**
     * @ORM\Column(type="text", length=4294967295, nullable=true)
     * @Gedmo\Versioned
     * @Assert\Length(
     *     max=65535,
     *     maxMessage="Content too long : {{ limit }} max"
     * )
     */
    private $content;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Gedmo\Versioned
     */
    private $amountMoneySettled;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    private $amountTimeSettled;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    private $amountCommunityWorkSettled;

    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->setNeedLegalAccess(true);
    }

    public function getDirectory(): ?Directory
    {
        return $this->directory;
    }

    public function setDirectory(?Directory $directory): self
    {
        $this->directory = $directory;

        return $this;
    }

    public function getAmountMoney(): ?float
    {
        return $this->amountMoney;
    }

    public function setAmountMoney(?float $amountMoney): self
    {
        $this->amountMoney = $amountMoney;

        return $this;
    }

    public function getAmountTime(): ?int
    {
        return $this->amountTime;
    }

    public function setAmountTime(?int $amountTime): self
    {
        $this->amountTime = $amountTime;

        return $this;
    }

    public function getAmountCommunityWork(): ?int
    {
        return $this->amountCommunityWork;
    }

    public function setAmountCommunityWork(?int $amountCommunityWork): self
    {
        $this->amountCommunityWork = $amountCommunityWork;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getAmountMoneySettled(): ?float
    {
        return $this->amountMoneySettled;
    }

    public function setAmountMoneySettled(?float $amountMoneySettled): self
    {
        $this->amountMoneySettled = $amountMoneySettled;

        return $this;
    }

    public function getAmountTimeSettled(): ?int
    {
        return $this->amountTimeSettled;
    }

    public function setAmountTimeSettled(?int $amountTimeSettled): self
    {
        $this->amountTimeSettled = $amountTimeSettled;

        return $this;
    }

    public function getAmountCommunityWorkSettled(): ?int
    {
        return $this->amountCommunityWorkSettled;
    }

    public function setAmountCommunityWorkSettled(?int $amountCommunityWorkSettled): self
    {
        $this->amountCommunityWorkSettled = $amountCommunityWorkSettled;

        return $this;
    }

    public function addSettlement($type, $amount)
    {
        if ($type == Criminal::TYPE_FINE) {
            if ($this->getAmountMoney() - $this->getAmountMoneySettled() < $amount) {
                throw new OutOfRangeException();
            }
            $this->setAmountMoneySettled($this->getAmountMoneySettled() + $amount);
        } elseif ($type == Criminal::TYPE_JAIL) {
            if ($this->getAmountTime() - $this->getAmountTimeSettled() < $amount) {
                throw new OutOfRangeException();
            }
            $this->setAmountTimeSettled($this->getAmountTimeSettled() + $amount);
        } elseif ($type == Criminal::TYPE_COMMUNITY_WORK) {
            if ($this->getAmountCommunityWork() - $this->getAmountCommunityWorkSettled() < $amount) {
                throw new OutOfRangeException();
            }
            $this->setAmountCommunityWorkSettled($this->getAmountCommunityWorkSettled() + $amount);
        } else {
            throw new OutOfRangeException();
        }

        return $this;
    }
}
