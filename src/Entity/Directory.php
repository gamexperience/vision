<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Repository\DirectoryRepository;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

use function Symfony\Component\String\u;

/**
 * @ORM\Entity(repositoryClass=DirectoryRepository::class)
 * @Vich\Uploadable
 * @Gedmo\Loggable
 */
class Directory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Versioned
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Versioned
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Gedmo\Versioned
     */
    private $gender;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Gedmo\Versioned
     */
    private $birthdate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    private $phone;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Versioned
     */
    private $address;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Gedmo\Versioned
     */
    private $height;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Gedmo\Versioned
     */
    private $weight;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @Gedmo\Timestampable(on="create")
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity=Jail::class, mappedBy="directory", orphanRemoval=true)
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $jails;

    /**
     * @Vich\UploadableField(mapping="directory_picture", fileNameProperty="faceImageName", size="faceImageSize")
     *
     * @var File|null
     *
     */
    private $faceImageFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Gedmo\Versioned
     *
     * @var string|null
     */
    private $faceImageName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var int|null
     */
    private $faceImageSize;

    /**
     * @Vich\UploadableField(mapping="directory_picture", fileNameProperty="backImageName", size="backImageSize")
     *
     * @var File|null
     *
     */
    private $backImageFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Gedmo\Versioned
     *
     * @var string|null
     */
    private $backImageName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var int|null
     */
    private $backImageSize;

    /**
     * @Vich\UploadableField(mapping="directory_picture", fileNameProperty="leftImageName", size="leftImageSize")
     *
     * @var File|null
     *
     */
    private $leftImageFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Gedmo\Versioned
     *
     * @var string|null
     */
    private $leftImageName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var int|null
     */
    private $leftImageSize;

    /**
     * @Vich\UploadableField(mapping="directory_picture", fileNameProperty="rightImageName", size="rightImageSize")
     *
     * @var File|null
     *
     */
    private $rightImageFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Gedmo\Versioned
     *
     * @var string|null
     */
    private $rightImageName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var int|null
     */
    private $rightImageSize;

    /**
     * @Vich\UploadableField(
     *  mapping="directory_picture",
     *  fileNameProperty="carLicenceImageName",
     *  size="carLicenceImageSize"
     * )
     *
     * @var File|null
     *
     */
    private $carLicenceImageFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Gedmo\Versioned
     *
     * @var string|null
     */
    private $carLicenceImageName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var int|null
     */
    private $carLicenceImageSize;

    /**
     * @Vich\UploadableField(
     *  mapping="directory_picture",
     *  fileNameProperty="motorcycleLicenceImageName",
     *  size="motorcycleLicenceImageSize"
     * )
     *
     * @var File|null
     *
     */
    private $motorcycleLicenceImageFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Gedmo\Versioned
     *
     * @var string|null
     */
    private $motorcycleLicenceImageName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var int|null
     */
    private $motorcycleLicenceImageSize;

    /**
     * @Vich\UploadableField(
     *  mapping="directory_picture",
     *  fileNameProperty="truckLicenceImageName",
     *  size="truckLicenceImageSize"
     * )
     *
     * @var File|null
     *
     */
    private $truckLicenceImageFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Gedmo\Versioned
     *
     * @var string|null
     */
    private $truckLicenceImageName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var int|null
     */
    private $truckLicenceImageSize;

    /**
     * @Vich\UploadableField(
     *  mapping="directory_picture",
     *  fileNameProperty="boatLicenceImageName",
     *  size="boatLicenceImageSize"
     * )
     *
     * @var File|null
     *
     */
    private $boatLicenceImageFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Gedmo\Versioned
     *
     * @var string|null
     */
    private $boatLicenceImageName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var int|null
     */
    private $boatLicenceImageSize;

    /**
     * @Vich\UploadableField(
     *  mapping="directory_picture",
     *  fileNameProperty="idCardImageName",
     *  size="idCardImageSize"
     * )
     *
     * @var File|null
     *
     */
    private $idCardImageFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Gedmo\Versioned
     *
     * @var string|null
     */
    private $idCardImageName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var int|null
     */
    private $idCardImageSize;

    /**
     * @ORM\OneToMany(targetEntity=Stolenvehicle::class, mappedBy="directory", orphanRemoval=true)
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $stolenvehicles;

    /**
     * @ORM\OneToMany(targetEntity=Certificate::class, mappedBy="directory", orphanRemoval=true)
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $certificates;

    /**
     * @ORM\OneToMany(targetEntity=Medical::class, mappedBy="directory", orphanRemoval=true)
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $medicals;

    /**
     * @ORM\OneToMany(targetEntity=Bracelet::class, mappedBy="directory", orphanRemoval=true)
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $bracelets;

    /**
     * @ORM\OneToMany(targetEntity=Criminal::class, mappedBy="directory", orphanRemoval=true)
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $criminals;

    /**
     * @ORM\OneToMany(targetEntity=Complaint::class, mappedBy="directory", orphanRemoval=true)
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $complaints;

    /**
     * @ORM\OneToMany(targetEntity=Licencewithdrawal::class, mappedBy="directory", orphanRemoval=true)
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $licencewithdrawals;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Versioned
     */
    private $medicalFamilyHistory;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Versioned
     */
    private $medicalHistory;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Versioned
     */
    private $medicalAllergies;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    private $medicalBloodGroup;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Versioned
     */
    private $medicalDrugs;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Versioned
     */
    private $medicalAlcohol;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Versioned
     */
    private $medicalTreatment;

    /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     * @Gedmo\Versioned
     */
    private $wanted = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    private $wantedReason;

    /**
     * @ORM\Column(type="boolean", options={"default":"1"})
     * @Gedmo\Versioned
     */
    private $wantedDisplay = true;

    /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     * @Gedmo\Versioned
     */
    private $wantedPublicDisplay = false;

    /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     * @Gedmo\Versioned
     */
    private $wantedPublicReason = false;

    /**
     * @ORM\OneToMany(targetEntity=Infringement::class, mappedBy="directory", orphanRemoval=true)
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $infringements;

    /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     * @Gedmo\Versioned
     */
    private $dead = false;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $faceImageDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $backImageDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $leftImageDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $rightImageDate;

    /**
     * @ORM\ManyToOne(targetEntity=Gang::class, inversedBy="directories")
     */
    private $gang;

    /**
     * @ORM\ManyToMany(targetEntity=Folder::class, mappedBy="directories")
     */
    private $folders;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    private $gangInfo;

    /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     * @Gedmo\Versioned
     */
    private $hasNoPapers = false;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Versioned
     */
    private $medicalContact;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Versioned
     */
    private $medicaltrusted;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Versioned
     */
    private $medicalLastWish;


    public function __construct()
    {
        $this->jails = new ArrayCollection();
        $this->stolenvehicles = new ArrayCollection();
        $this->certificates = new ArrayCollection();
        $this->medicals = new ArrayCollection();
        $this->bracelets = new ArrayCollection();
        $this->criminals = new ArrayCollection();
        $this->complaints = new ArrayCollection();
        $this->licencewithdrawals = new ArrayCollection();
        $this->infringements = new ArrayCollection();
        $this->folders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFullName(): string
    {
        return sprintf('%s %s', u($this->firstname)->title(true), u($this->lastname)->upper());
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getBirthdate(): ?\DateTimeInterface
    {
        return $this->birthdate;
    }

    public function setBirthdate(?\DateTimeInterface $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function setHeight(?int $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(?float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|Jail[]
     */
    public function getJails(): Collection
    {
        return $this->jails;
    }

    public function addJail(Jail $jail): self
    {
        if (!$this->jails->contains($jail)) {
            $this->jails[] = $jail;
            $jail->setDirectory($this);
        }

        return $this;
    }

    public function removeJail(Jail $jail): self
    {
        if ($this->jails->removeElement($jail)) {
            // set the owning side to null (unless already changed)
            if ($jail->getDirectory() === $this) {
                $jail->setDirectory(null);
            }
        }

        return $this;
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $faceImageFile
     */
    public function setFaceImageFile(?File $faceImageFile = null): void
    {
        $this->faceImageFile = $faceImageFile;

        if (null !== $faceImageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = $this->faceImageDate = new \DateTimeImmutable();
        }
    }

    public function getFaceImageFile(): ?File
    {
        return $this->faceImageFile;
    }

    public function setFaceImageName(?string $faceImageName): void
    {
        $this->faceImageName = $faceImageName;
    }

    public function getFaceImageName(): ?string
    {
        return $this->faceImageName;
    }

    public function setFaceImageSize(?int $faceImageSize): void
    {
        $this->faceImageSize = $faceImageSize;
    }

    public function getFaceImageSize(): ?int
    {
        return $this->faceImageSize;
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $backImageFile
     */
    public function setBackImageFile(?File $backImageFile = null): void
    {
        $this->backImageFile = $backImageFile;

        if (null !== $backImageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = $this->backImageDate = new \DateTimeImmutable();
        }
    }

    public function getBackImageFile(): ?File
    {
        return $this->backImageFile;
    }

    public function setBackImageName(?string $backImageName): void
    {
        $this->backImageName = $backImageName;
    }

    public function getBackImageName(): ?string
    {
        return $this->backImageName;
    }

    public function setBackImageSize(?int $backImageSize): void
    {
        $this->backImageSize = $backImageSize;
    }

    public function getBackImageSize(): ?int
    {
        return $this->backImageSize;
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $leftImageFile
     */
    public function setLeftImageFile(?File $leftImageFile = null): void
    {
        $this->leftImageFile = $leftImageFile;

        if (null !== $leftImageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = $this->leftImageDate = new \DateTimeImmutable();
        }
    }

    public function getLeftImageFile(): ?File
    {
        return $this->leftImageFile;
    }

    public function setLeftImageName(?string $leftImageName): void
    {
        $this->leftImageName = $leftImageName;
    }

    public function getLeftImageName(): ?string
    {
        return $this->leftImageName;
    }

    public function setLeftImageSize(?int $leftImageSize): void
    {
        $this->leftImageSize = $leftImageSize;
    }

    public function getLeftImageSize(): ?int
    {
        return $this->leftImageSize;
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $rightImageFile
     */
    public function setRightImageFile(?File $rightImageFile = null): void
    {
        $this->rightImageFile = $rightImageFile;

        if (null !== $rightImageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = $this->rightImageDate = new \DateTimeImmutable();
        }
    }

    public function getRightImageFile(): ?File
    {
        return $this->rightImageFile;
    }

    public function setRightImageName(?string $rightImageName): void
    {
        $this->rightImageName = $rightImageName;
    }

    public function getRightImageName(): ?string
    {
        return $this->rightImageName;
    }

    public function setRightImageSize(?int $rightImageSize): void
    {
        $this->rightImageSize = $rightImageSize;
    }

    public function getRightImageSize(): ?int
    {
        return $this->rightImageSize;
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $carLicenceImageFile
     */
    public function setCarLicenceImageFile(?File $carLicenceImageFile = null): void
    {
        $this->carLicenceImageFile = $carLicenceImageFile;

        if (null !== $carLicenceImageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getCarLicenceImageFile(): ?File
    {
        return $this->carLicenceImageFile;
    }

    public function setCarLicenceImageName(?string $carLicenceImageName): void
    {
        $this->carLicenceImageName = $carLicenceImageName;
    }

    public function getCarLicenceImageName(): ?string
    {
        return $this->carLicenceImageName;
    }

    public function setCarLicenceImageSize(?int $carLicenceImageSize): void
    {
        $this->carLicenceImageSize = $carLicenceImageSize;
    }

    public function getCarLicenceImageSize(): ?int
    {
        return $this->carLicenceImageSize;
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $motorcycleLicenceImageFile
     */
    public function setMotorcycleLicenceImageFile(?File $motorcycleLicenceImageFile = null): void
    {
        $this->motorcycleLicenceImageFile = $motorcycleLicenceImageFile;

        if (null !== $motorcycleLicenceImageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getMotorcycleLicenceImageFile(): ?File
    {
        return $this->motorcycleLicenceImageFile;
    }

    public function setMotorcycleLicenceImageName(?string $motorcycleLicenceImageName): void
    {
        $this->motorcycleLicenceImageName = $motorcycleLicenceImageName;
    }

    public function getMotorcycleLicenceImageName(): ?string
    {
        return $this->motorcycleLicenceImageName;
    }

    public function setMotorcycleLicenceImageSize(?int $motorcycleLicenceImageSize): void
    {
        $this->motorcycleLicenceImageSize = $motorcycleLicenceImageSize;
    }

    public function getMotorcycleLicenceImageSize(): ?int
    {
        return $this->motorcycleLicenceImageSize;
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $truckLicenceImageFile
     */
    public function setTruckLicenceImageFile(?File $truckLicenceImageFile = null): void
    {
        $this->truckLicenceImageFile = $truckLicenceImageFile;

        if (null !== $truckLicenceImageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getTruckLicenceImageFile(): ?File
    {
        return $this->truckLicenceImageFile;
    }

    public function setTruckLicenceImageName(?string $truckLicenceImageName): void
    {
        $this->truckLicenceImageName = $truckLicenceImageName;
    }

    public function getTruckLicenceImageName(): ?string
    {
        return $this->truckLicenceImageName;
    }

    public function setTruckLicenceImageSize(?int $truckLicenceImageSize): void
    {
        $this->truckLicenceImageSize = $truckLicenceImageSize;
    }

    public function getTruckLicenceImageSize(): ?int
    {
        return $this->truckLicenceImageSize;
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $boatLicenceImageFile
     */
    public function setBoatLicenceImageFile(?File $boatLicenceImageFile = null): void
    {
        $this->boatLicenceImageFile = $boatLicenceImageFile;

        if (null !== $boatLicenceImageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getBoatLicenceImageFile(): ?File
    {
        return $this->boatLicenceImageFile;
    }

    public function setBoatLicenceImageName(?string $boatLicenceImageName): void
    {
        $this->boatLicenceImageName = $boatLicenceImageName;
    }

    public function getBoatLicenceImageName(): ?string
    {
        return $this->boatLicenceImageName;
    }

    public function setBoatLicenceImageSize(?int $boatLicenceImageSize): void
    {
        $this->boatLicenceImageSize = $boatLicenceImageSize;
    }

    public function getBoatLicenceImageSize(): ?int
    {
        return $this->boatLicenceImageSize;
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $idCardImageFile
     */
    public function setIdCardImageFile(?File $idCardImageFile = null): void
    {
        $this->idCardImageFile = $idCardImageFile;

        if (null !== $idCardImageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getIdCardImageFile(): ?File
    {
        return $this->idCardImageFile;
    }

    public function setIdCardImageName(?string $idCardImageName): void
    {
        $this->idCardImageName = $idCardImageName;
    }

    public function getIdCardImageName(): ?string
    {
        return $this->idCardImageName;
    }

    public function setIdCardImageSize(?int $idCardImageSize): void
    {
        $this->idCardImageSize = $idCardImageSize;
    }

    public function getIdCardImageSize(): ?int
    {
        return $this->idCardImageSize;
    }

    /**
     * @return Collection|Stolenvehicle[]
     */
    public function getStolenvehicles(): Collection
    {
        return $this->stolenvehicles;
    }

    public function addStolenvehicle(Stolenvehicle $stolenvehicle): self
    {
        if (!$this->stolenvehicles->contains($stolenvehicle)) {
            $this->stolenvehicles[] = $stolenvehicle;
            $stolenvehicle->setDirectory($this);
        }

        return $this;
    }

    public function removeStolenvehicle(Stolenvehicle $stolenvehicle): self
    {
        if ($this->stolenvehicles->removeElement($stolenvehicle)) {
            // set the owning side to null (unless already changed)
            if ($stolenvehicle->getDirectory() === $this) {
                $stolenvehicle->setDirectory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Certificate[]
     */
    public function getCertificates(): Collection
    {
        return $this->certificates;
    }

    public function addCertificate(Certificate $certificate): self
    {
        if (!$this->certificates->contains($certificate)) {
            $this->certificates[] = $certificate;
            $certificate->setDirectory($this);
        }

        return $this;
    }

    public function removeCertificate(Certificate $certificate): self
    {
        if ($this->certificates->removeElement($certificate)) {
            // set the owning side to null (unless already changed)
            if ($certificate->getDirectory() === $this) {
                $certificate->setDirectory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Medical[]
     */
    public function getMedicals(): Collection
    {
        return $this->medicals;
    }

    public function addMedical(Medical $medical): self
    {
        if (!$this->medicals->contains($medical)) {
            $this->medicals[] = $medical;
            $medical->setDirectory($this);
        }

        return $this;
    }

    public function removeMedical(Medical $medical): self
    {
        if ($this->medicals->removeElement($medical)) {
            // set the owning side to null (unless already changed)
            if ($medical->getDirectory() === $this) {
                $medical->setDirectory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Bracelet[]
     */
    public function getBracelets(): Collection
    {
        return $this->bracelets;
    }

    public function addBracelet(Bracelet $bracelet): self
    {
        if (!$this->bracelets->contains($bracelet)) {
            $this->bracelets[] = $bracelet;
            $bracelet->setDirectory($this);
        }

        return $this;
    }

    public function removeBracelet(Bracelet $bracelet): self
    {
        if ($this->bracelets->removeElement($bracelet)) {
            // set the owning side to null (unless already changed)
            if ($bracelet->getDirectory() === $this) {
                $bracelet->setDirectory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Criminal[]
     */
    public function getCriminals(): Collection
    {
        return $this->criminals;
    }

    public function addCriminal(Criminal $criminal): self
    {
        if (!$this->criminals->contains($criminal)) {
            $this->criminals[] = $criminal;
            $criminal->setDirectory($this);
        }

        return $this;
    }

    public function removeCriminal(Criminal $criminal): self
    {
        if ($this->criminals->removeElement($criminal)) {
            // set the owning side to null (unless already changed)
            if ($criminal->getDirectory() === $this) {
                $criminal->setDirectory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Complaint[]
     */
    public function getComplaints(): Collection
    {
        return $this->complaints;
    }

    public function addComplaint(Complaint $complaint): self
    {
        if (!$this->complaints->contains($complaint)) {
            $this->complaints[] = $complaint;
            $complaint->setDirectory($this);
        }

        return $this;
    }

    public function removeComplaint(Complaint $complaint): self
    {
        if ($this->complaints->removeElement($complaint)) {
            // set the owning side to null (unless already changed)
            if ($complaint->getDirectory() === $this) {
                $complaint->setDirectory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Licencewithdrawal[]
     */
    public function getLicencewithdrawals(): Collection
    {
        return $this->licencewithdrawals;
    }

    public function addLicencewithdrawal(Licencewithdrawal $licencewithdrawal): self
    {
        if (!$this->licencewithdrawals->contains($licencewithdrawal)) {
            $this->licencewithdrawals[] = $licencewithdrawal;
            $licencewithdrawal->setDirectory($this);
        }

        return $this;
    }

    public function removeLicencewithdrawal(Licencewithdrawal $licencewithdrawal): self
    {
        if ($this->licencewithdrawals->removeElement($licencewithdrawal)) {
            // set the owning side to null (unless already changed)
            if ($licencewithdrawal->getDirectory() === $this) {
                $licencewithdrawal->setDirectory(null);
            }
        }

        return $this;
    }

    public function getMedicalFamilyHistory(): ?string
    {
        return $this->medicalFamilyHistory;
    }

    public function setMedicalFamilyHistory(?string $medicalFamilyHistory): self
    {
        $this->medicalFamilyHistory = $medicalFamilyHistory;

        return $this;
    }

    public function getMedicalHistory(): ?string
    {
        return $this->medicalHistory;
    }

    public function setMedicalHistory(?string $medicalHistory): self
    {
        $this->medicalHistory = $medicalHistory;

        return $this;
    }

    public function getMedicalAllergies(): ?string
    {
        return $this->medicalAllergies;
    }

    public function setMedicalAllergies(?string $medicalAllergies): self
    {
        $this->medicalAllergies = $medicalAllergies;

        return $this;
    }

    public function getMedicalBloodGroup(): ?string
    {
        return $this->medicalBloodGroup;
    }

    public function setMedicalBloodGroup(?string $medicalBloodGroup): self
    {
        $this->medicalBloodGroup = $medicalBloodGroup;

        return $this;
    }

    public function getMedicalDrugs(): ?string
    {
        return $this->medicalDrugs;
    }

    public function setMedicalDrugs(?string $medicalDrugs): self
    {
        $this->medicalDrugs = $medicalDrugs;

        return $this;
    }

    public function getMedicalAlcohol(): ?string
    {
        return $this->medicalAlcohol;
    }

    public function setMedicalAlcohol(?string $medicalAlcohol): self
    {
        $this->medicalAlcohol = $medicalAlcohol;

        return $this;
    }

    public function getMedicalTreatment(): ?string
    {
        return $this->medicalTreatment;
    }

    public function setMedicalTreatment(?string $medicalTreatment): self
    {
        $this->medicalTreatment = $medicalTreatment;

        return $this;
    }

    public function getWanted(): ?bool
    {
        return $this->wanted;
    }

    public function setWanted(bool $wanted): self
    {
        $this->wanted = $wanted;

        return $this;
    }

    public function getWantedReason(): ?string
    {
        return $this->wantedReason;
    }

    public function setWantedReason(?string $wantedReason): self
    {
        $this->wantedReason = $wantedReason;

        return $this;
    }

    public function getWantedDisplay(): ?bool
    {
        return $this->wantedDisplay;
    }

    public function setWantedDisplay(bool $wantedDisplay): self
    {
        $this->wantedDisplay = $wantedDisplay;

        return $this;
    }

    public function getWantedPublicDisplay(): ?bool
    {
        return $this->wantedPublicDisplay;
    }

    public function setWantedPublicDisplay(bool $wantedPublicDisplay): self
    {
        $this->wantedPublicDisplay = $wantedPublicDisplay;

        return $this;
    }

    public function getWantedPublicReason(): ?bool
    {
        return $this->wantedPublicReason;
    }

    public function setWantedPublicReason(bool $wantedPublicReason): self
    {
        $this->wantedPublicReason = $wantedPublicReason;

        return $this;
    }

    /**
     * @return Collection|Infringement[]
     */
    public function getInfringements(): Collection
    {
        return $this->infringements;
    }

    public function addInfringement(Infringement $infringement): self
    {
        if (!$this->infringements->contains($infringement)) {
            $this->infringements[] = $infringement;
            $infringement->setDirectory($this);
        }

        return $this;
    }

    public function removeInfringement(Infringement $infringement): self
    {
        if ($this->infringements->removeElement($infringement)) {
            // set the owning side to null (unless already changed)
            if ($infringement->getDirectory() === $this) {
                $infringement->setDirectory(null);
            }
        }

        return $this;
    }

    public function getDead(): ?bool
    {
        return $this->dead;
    }

    public function setDead(bool $dead): self
    {
        $this->dead = $dead;

        return $this;
    }

    public function getFaceImageDate(): ?\DateTimeInterface
    {
        return $this->faceImageDate;
    }

    public function setFaceImageDate(?\DateTimeInterface $faceImageDate): self
    {
        $this->faceImageDate = $faceImageDate;

        return $this;
    }

    public function getBackImageDate(): ?\DateTimeInterface
    {
        return $this->backImageDate;
    }

    public function setBackImageDate(?\DateTimeInterface $backImageDate): self
    {
        $this->backImageDate = $backImageDate;

        return $this;
    }

    public function getLeftImageDate(): ?\DateTimeInterface
    {
        return $this->leftImageDate;
    }

    public function setLeftImageDate(?\DateTimeInterface $leftImageDate): self
    {
        $this->leftImageDate = $leftImageDate;

        return $this;
    }

    public function getRightImageDate(): ?\DateTimeInterface
    {
        return $this->rightImageDate;
    }

    public function setRightImageDate(?\DateTimeInterface $rightImageDate): self
    {
        $this->rightImageDate = $rightImageDate;

        return $this;
    }

    public function getGang(): ?Gang
    {
        return $this->gang;
    }

    public function setGang(?Gang $gang): self
    {
        $this->gang = $gang;

        return $this;
    }

    /**
     * @return Collection|Folder[]
     */
    public function getFolders(): Collection
    {
        return $this->folders;
    }

    public function addFolder(Folder $folder): self
    {
        if (!$this->folders->contains($folder)) {
            $this->folders[] = $folder;
            $folder->addDirectory($this);
        }

        return $this;
    }

    public function removeFolder(Folder $folder): self
    {
        if ($this->folders->removeElement($folder)) {
            $folder->removeDirectory($this);
        }

        return $this;
    }

    public function getGangInfo(): ?string
    {
        return $this->gangInfo;
    }

    public function setGangInfo(?string $gangInfo): self
    {
        $this->gangInfo = $gangInfo;

        return $this;
    }

    public function getHasNoPapers(): ?bool
    {
        return $this->hasNoPapers;
    }

    public function setHasNoPapers(bool $hasNoPapers): self
    {
        $this->hasNoPapers = $hasNoPapers;

        return $this;
    }

    public function getMedicalContact(): ?string
    {
        return $this->medicalContact;
    }

    public function setMedicalContact(?string $medicalContact): self
    {
        $this->medicalContact = $medicalContact;

        return $this;
    }

    public function getMedicaltrusted(): ?string
    {
        return $this->medicaltrusted;
    }

    public function setMedicaltrusted(?string $medicaltrusted): self
    {
        $this->medicaltrusted = $medicaltrusted;

        return $this;
    }

    public function getMedicalLastWish(): ?string
    {
        return $this->medicalLastWish;
    }

    public function setMedicalLastWish(?string $medicalLastWish): self
    {
        $this->medicalLastWish = $medicalLastWish;

        return $this;
    }
}
