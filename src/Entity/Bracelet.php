<?php

namespace App\Entity;

use DateTimeImmutable;
use App\Entity\Document;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\BraceletRepository;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=BraceletRepository::class)
 */
class Bracelet extends Document
{
    /**
     * @ORM\ManyToOne(targetEntity=Directory::class, inversedBy="bracelets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $directory;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @Gedmo\Versioned
     */
    private $removingDate;

    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->setNeedLegalAccess(true);
        $this->setRemovingDate(new DateTimeImmutable());
    }

    public function getDirectory(): ?Directory
    {
        return $this->directory;
    }

    public function setDirectory(?Directory $directory): self
    {
        $this->directory = $directory;

        return $this;
    }

    public function getRemovingDate(): ?\DateTimeImmutable
    {
        return $this->removingDate;
    }

    public function setRemovingDate(\DateTimeImmutable $removingDate): self
    {
        $this->removingDate = $removingDate;

        return $this;
    }
}
