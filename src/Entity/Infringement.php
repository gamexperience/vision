<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Repository\InfringementRepository;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=InfringementRepository::class)
 */
class Infringement extends Document
{
    /**
     * @ORM\Column(type="text", length=4294967295)
     * @Gedmo\Versioned
     * @Assert\Length(
     *     max=65535,
     *     maxMessage="Content too long : {{ limit }} max"
     * )
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity=Directory::class, inversedBy="infringements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $directory;

    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->setNeedLegalAccess(true);
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getDirectory(): ?Directory
    {
        return $this->directory;
    }

    public function setDirectory(?Directory $directory): self
    {
        $this->directory = $directory;

        return $this;
    }
}
