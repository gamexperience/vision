<?php

namespace App\DataFixtures;

use App\Entity\Group;
use App\Entity\Rank;
use App\Entity\SubGroup;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class Groups extends Fixture
{
    public function load(ObjectManager $manager): void
    {

        $groups = [
            [
                'name' => 'Los Santos Police Department',
                'shortname' => 'LSPD',
                'ranks' => [
                    [
                        'name' => 'Captain',
                        'shortname' => 'Cpt',
                        'power' => 999
                    ],
                    [
                        'name' => 'Officer',
                        'shortname' => 'Off',
                        'power' => 111
                    ],
                ],
                'subgroups' => [
                    [
                        'name' => 'Detective Unit',
                        'shortname' => 'DU'
                    ],
                    [
                        'name' => 'Speed Unit',
                        'shortname' => 'SU'
                    ],
                ],
            ],

            [
                'name' => 'Department of Justice',
                'shortname' => 'DOJ',
                'ranks' => [
                    [
                        'name' => 'Judge',
                        'shortname' => 'Judge',
                        'power' => 999
                    ],
                    [
                        'name' => 'Attorney',
                        'shortname' => 'Att',
                        'power' => 888
                    ],
                ],

            ],
            [
                'name' => 'Emergency Medical Services',
                'shortname' => 'EMS',
                'ranks' => [
                    [
                        'name' => 'Director',
                        'shortname' => 'Dir',
                        'power' => 999
                    ],
                    [
                        'name' => 'Doctor',
                        'shortname' => 'Doc',
                        'power' => 888
                    ],
                ],
                'subgroups' => [
                    [
                        'name' => 'Psychology',
                        'shortname' => 'PSY'
                    ],
                    [
                        'name' => 'legal unit',
                        'shortname' => 'LEG'
                    ],
                ],

            ],
        ];

        foreach ($groups as $key => $group) {
            $NewGroup = new Group();
            $NewGroup->setName($group['name']);
            $NewGroup->setShortName($group['shortname']);
            foreach ($group['ranks'] as $key => $rank) {
                $newRank = new Rank();
                $newRank->setName($rank['name']);
                $newRank->setShortName($rank['shortname']);
                $newRank->setPower($rank['power']);
                $manager->persist($newRank);
                $NewGroup->addRank($newRank);
            }

            if (array_key_exists('subgroups', $group)) :
                foreach ($group['subgroups'] as $key => $subGroup) {
                    $newSubGroup = new SubGroup();
                    $newSubGroup->setName($subGroup['name']);
                    $newSubGroup->setShortName($subGroup['shortname']);
                    $manager->persist($newSubGroup);
                    $NewGroup->addSubGroup($newSubGroup);
                }
            endif;

            $manager->persist($NewGroup);
        }

        $manager->flush();
    }
}
