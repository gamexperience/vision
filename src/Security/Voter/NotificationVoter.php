<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class NotificationVoter extends Voter
{
    protected function supports(string $attribute, $subject): bool
    {

        return in_array($attribute, ['delete', 'markread'])
            && $subject instanceof \App\Entity\Notification;
    }
    /**
     * vote based on Notification
     *
     * @param string $attribute
     * @param Notification $subject
     * @param TokenInterface $token
     * @return boolean
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'delete':
            case 'markread':
                if ($subject->getReceiver() == $user) {
                    return true;
                }
                break;
        }

        return false;
    }
}
